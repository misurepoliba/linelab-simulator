%% Init
clear, clc

disp('LineLab >> Time Domain simulation module')
addpath('./lib')

%% Setting up filenames and loading needed data
line_file = './lines/line.mat';
measurement_file = './measurements/lecroy/1_i2mtc_m_10pF.mat';

disp('LineLab >> Loading simulated line parameters')
load(line_file)

%% Set stimulus signal
% This cell may be used to override the default stimulus provided by the
% datafile containing the line response. Uncomment ONLY the desired mode

%stimulusMode = 'datafile'; % Default stimulus or sythStimulus if none provided in the line file
stimulusMode = 'synthetic'; % Synthetic stimulus with parameters given in the synthStimulus struct
%stimulusMode = 'measured'; % Overrides the default stimulus with another measured signal loaded from the stimulusOverride path

% Check a list of available signal shapes in the help of TLsourceSignal
% Delay and width factors are referred to the whole simulation time
synthStimulus = struct('shape', 'gauss_pulse', ...
    'delayfactor', 0.1, 'widthfactor', 0.01, ...
    'amplitude', 1);

%% Signal loading or synthesis
disp('LineLab >> Configuring stimulus signal')

switch stimulusMode
    case 'datafile'
        wave = load(sim_params.stimulusfile);
        tsig = wave.t;
        Vs = wave.v;
        
    case 'synthetic'
        tsig = freq_params.t; tsig = tsig - tsig(1);
        
        % Extracting relevant line parameters
        % TODO adapt to multiline
        line_velocity = sim_params.v_line;
        line_length = 0;
        for i = 1:length(network_topology)
            line_length = line_length + network_topology{i}.l;
        end
        t_expected = 2*line_length/line_velocity;
        
        Vs = TLsourceSignal(synthStimulus.shape, tsig, ...
            synthStimulus.delayfactor * t_expected, synthStimulus.widthfactor * t_expected, ...
            synthStimulus.amplitude);
    case 'measured'
        wave = load(sim_params.stimulusfile);
        tsig = wave.t;
        Vs = wave.v;
        
    otherwise
        error('LineLab - TDR simulation module: invalid stimulus mode')
end

%% Time domain response computation (FFT)
disp('LineLab >> Computing TDR response')
% TODO implement Fourier analysis consistency validation

% Avoiding odd number of samples
if mod(length(tsig), 2)
    tsig = tsig(1:end-1);
    Vs = Vs(1:end-1);
end

f = freq_params.omega/(2*pi);
f_max = freq_params.fmax;

if length(tsig) ~= 2*(freq_params.Nh+1)
    % Resampling needed
    df_required = 1/range(tsig);
    Nh_required = round(f_max / df_required);
    f_required = df_required*(1:Nh_required)';

    Nt_required = 2*(Nh_required + 1);
    t_ = linspace(0, tsig(end), Nt_required); dt = mean(diff(t_)); % Time array and dt
    Vs = interp1(tsig, Vs, t_); Vs = Vs';

    VrefH = interp1(f, VrefH, f_required, 'spline');
else
    Nt = 2*(freq_params.Nh+1);
end

Vs = reshape(Vs, [length(Vs), 1]);

Vs_fft = fft(Vs)/Nt;

H = [VrefH; 0; conj(VrefH(end:-1:2))];
xRef_fft = H .* Vs_fft;
xRef = ifft(xRef_fft)*Nt;

%% Integrated time-domain response
% FIXME find a more orthodox way to remove the baseline
% xRef = xRef - mean(xRef(1:50));

% xRefInt = TLfftIntegral(tsig, xRef);
% 
% % FIXME raw normalization steps, to be removed
% xRefInt = xRefInt - xRefInt(1);
% xRefInt = xRefInt / max(xRefInt);

%% Plot time domain response
meas_file = load(measurement_file);
vmeas = meas_file.v/2;
hTDR = TLplotTDR(tsig, xRef/max(xRef), 'fontsize', 12, 'linewidth', 2, 'xlim', [0, 1.8e-6]);
%hTDR = TLplotTDR(tsig, xRef, 'measurements', vmeas, 'fontsize', 12, 'linewidth', 1, 'xlim', [-2, 60]*1e-9);
%hTDR = TLplotTDR(tsig, xRef, 'measurements', vmeas, 'fontsize', 12, 'linewidth', 2);

%% Ending program

rmpath('./lib')

disp('LineLab >> Time domain simulation completed')
