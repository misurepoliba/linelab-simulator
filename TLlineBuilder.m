%% Init
clear, close all
clc

% File path and name for line model
savefile = './line_models/rg58cu_1m.mat';

%% Define the model and save it
% Define any number of chained lines and put them in the line_model cell
% array
rg58 = struct( ...
    'linemodel', 'coax', 'ri', 0.455e-3, 'ro', 1.475e-3, 't', 0.007e-3, 'l', 1, ... % Line model and geometry
    'epsilon_r', 2.26, 'esrtoggle', true, 'tan_d', 0.00031, 'debyetoggle', false, ... % Dielectric properties
    'sigma', 5.96e7, 'skintoggle', true, ... % Conductor properties
    'Lprofile', struct('shape', 'disabled'), ...
    'Rprofile', struct('shape', 'disabled'), ...
    'Cprofile', struct('shape', 'disabled'), ...
    'Gprofile', struct('shape', 'disabled') ...
);

line_model = {rg58};

save(savefile, 'line_model')