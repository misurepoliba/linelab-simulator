%% Init
clear, close all
clc

tic % Start execution timer

% Measured stimuli
stimulus_file_1 = './measurements/tek/20170505_tek_s_50ohm.mat';
stimulus_file_2 = './measurements/tek/20170505_tek_s_25ohm.mat';

% Measured load impedances
Zterm = load('./measurements/vna/50ohm.mat');

calibrated_filename = './measurements/tek/tek_s_cal.mat'; % Save calibrated stimulus and connector model

% Port parameters

% Load (measurement port) impedances
RL = 50; CL = 5e-12;
ZL_1 = @(w) RL ./ (1+ 1i*w*RL*CL);

Z50 = @(w) interp1(Zterm.f, Zterm.Z, w/2/pi, 'spline', 'extrap');
ZL_2 = @(w) Z50(w) .* ZL_1(w) ./ (Z50(w) + ZL_1(w));

disp('LineLab >> Transmission matrix computation module')
addpath('./lib')

%% Basic line parameters

% Line parameters (check TLprimaryParameters documentation)
sma_coax = struct( ...
    'linemodel', 'coax', 'ri', 0.255e-3, 'ro', 0.76e-3, 't', 0.007e-3, 'l', 0.40, ... % Line model and geometry
    'epsilon_r', 2.1, 'esrtoggle', true, 'tan_d', 0.001, 'debyetoggle', false, ... % Dielectric properties
    'skintoggle', true, ... % Skin effect
    'Lprofile', struct('shape', 'disabled'), ...
    'Rprofile', struct('shape', 'disabled'), ...
    'Cprofile', struct('shape', 'disabled'), ...
    'Gprofile', struct('shape', 'disabled') ...
);

connectors = {sma_coax};

[L0_line , R0_line, C0_line, G0_line, l] = TLprimaryParameters(connectors);

% Approximate propagation velocity
v_line = max(sqrt(1 ./ (L0_line .* C0_line))); % Propagation velocity [m/s]

%% Simulation parameters
% Also check TLfrequencySpan documentation)
sim_params = struct( ...
    'freqmode', 'signal',...
    'stimulusfile', stimulus_file_1,...
    'v_line', v_line, ...
    'l', sum(l) ...
);

%% Simulation parameters
freq_params = TLfrequencySpan(sim_params);

% Array length check
if freq_params.Nx >= 20e3
    array_check = questdlg(sprintf('LineLab >> Current settings require the simulation of %d cells. Continue anyway?', freq_params.Nx), 'LineLab long array');
    if ~strcmp(array_check, 'Yes')
        disp('LineLab >> Simulation aborted');
        return
    end
end

%% Profiling parameters

disp('LineLab >> Computing custom profiles for primary parameters')

% Extracting relevant quantities from freqParams struct
Nh = freq_params.Nh;
Nx = freq_params.Nx;
dx = freq_params.dx;
omega = freq_params.omega;

% Computing parameter profiles
Nxi = round(Nx*l/sum(l));
% FIXME find a cleaner solution to ensure sum(Nxi) = Nx
if sum(Nxi) - Nx ~= 0
    Nxi(1) = Nxi(1) - (sum(Nxi) - Nx);
end

L_line = zeros(1,Nx);
C_line = zeros(1,Nx);
R_line = zeros(1,Nx);
G_line = zeros(1,Nx);
for i = 1:length(Nxi)
    if i == 1
        istart = 1;
    else
        istart = Nxi(i-1)+1;
    end

    L_line(istart:istart+Nxi(i)-1) = L0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Lprofile);
    R_line(istart:istart+Nxi(i)-1) = R0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Rprofile);
    C_line(istart:istart+Nxi(i)-1) = C0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Cprofile);
    G_line(istart:istart+Nxi(i)-1) = G0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Gprofile);
end

%% Solution (numeric)
% Performing frequency domain simulations
disp('LineLab >> Performing frequency domain simulations')

z11 = zeros(Nh+1, 1); z12 = zeros(Nh+1, 1);
z21 = zeros(Nh+1, 1); z22 = zeros(Nh+1, 1);
parfor kw = 1:Nh
    % Series dispersion
    YL = TLskinEffect(Nxi, connectors, omega(kw), R_line, L_line) / dx;
    % Parallel dispersion
    YC = TLepsilonDispersion(Nxi, connectors, omega(kw), G_line, C_line) * dx;
    
    [z11(kw+1), z12(kw+1), z21(kw+1), z22(kw+1)] = TLimpedanceMatrix(YL, YC);
end

% DC response
if sum(R_line) ~= 0
    [z11(1), z12(1), z21(1), z22(1)] = TLimpedanceMatrix(1./R_line, G_line);
else
    % TODO not implemented
end

stopwatch = toc;
fprintf('LineLab >> Impedance matrix computed in %d min %d sec\n', ...
    floor(stopwatch/60), round(mod(stopwatch, 60)))

%% Stimulus calibration procedure
% This script estimates the true stimulus, calibrating out mismatches
% between generator, connector and probe end

stimulus_1 = load(stimulus_file_1);
stimulus_2 = load(stimulus_file_2);

t = stimulus_1.t; % Timebase

% Measured stimuli
v2_1 = stimulus_1.v;
v2_2 = stimulus_2.v;

%% Into the frequency domain

% Array of frequencies
omega = [0; omega];

% Load impedances
ZL_1_w = ZL_1(omega);
ZL_2_w = ZL_2(omega);

%% Calibration
Nt = length(v2_1);

V2_1 = fft(v2_1)/Nt;
V2_1 = V2_1(1:end/2); V2_1(2:end) = 2*V2_1(2:end);

V2_2 = fft(v2_2)/Nt;
V2_2 = V2_2(1:end/2); V2_2(2:end) = 2*V2_2(2:end);

k1 = (ZL_1_w + z22) ./ (z21 .* ZL_1_w); h1 = z12 ./ ZL_1_w;
k2 = (ZL_2_w + z22) ./ (z21 .* ZL_2_w); h2 = z12 ./ ZL_2_w;

A = k1 .* V2_1 ./ (k2 .* V2_2);

VS = (z11 .* k1 .* V2_1 - h1 .* V2_1 - A .* z11 .* k2.* V2_2 + A .* h2 .* V2_2) ./ (1 - A);

Zsource = (VS - z11 .* k1 .* V2_1 + h1 .* V2_1) ./ (k1 .* V2_1);

VS = [VS(1); VS(2:end)/2; 0; conj(VS(end:-1:2))/2];

% FIXME ugly workaround for DC
VS(1) = 0;
Zsource(1) = real(Zsource(2));

v = ifft(VS) * length(VS);

%% Plot calibrated stimulus
plot(t, v, t, v2_1, t, v2_2)
delay = mean(diff(t)) * finddelay(v, v2_2);
grid on
%xlim([-4,4]*1e-9)
xlabel('t [s]', 'fontsize', 12, 'interpreter', 'latex')
ylabel('Waveform [V]', 'fontsize', 12, 'interpreter', 'latex')
legend({'Calibrated', 'Measured (1)', 'Measured (2)'}, 'fontsize', 12, 'interpreter', 'latex', 'location', 'best')
title(['Estimated delay: ', num2str(delay, '%.3e'), ' s'], 'fontsize', 12, 'interpreter', 'latex')

%% Save calibrated stimulus
omega_s = omega;
vars = {'t', 'v', 'Zsource', 'omega_s', 'connectors'};
save(calibrated_filename, vars{:})

rmpath('./lib')
