%% Init
clear, close all
clc

tic % Start execution timer

stimulus_file = './measurements/tek/20170505_tek_s_50ohm.mat'; % Measured stimulus
calibrated_filename = './measurements/tek/tek_s_cal.mat'; % Save calibrated stimulus and connector model

% Port parameters

% Source impedance
RS = 57; CS = 0e-12; LS = 0e-9;
ZS = @(w) (RS + 1i*w*LS) ./ (1 + 1i*w*RS*CS - w.^2*LS*CS);

% Load (measurement port) impedance
RL = 50; CL = 5e-12;
ZL = @(w) RL ./ (1+ 1i*w*RL*CL);

disp('LineLab >> Transmission matrix computation module')
addpath('./lib')

%% Basic line parameters

% Line parameters (check TLprimaryParameters documentation)
sma_coax = struct( ...
    'linemodel', 'coax', 'ri', 0.255e-3, 'ro', 0.76e-3, 't', 0.007e-3, 'l', 0.4, ... % Line model and geometry
    'epsilon_r', 2.1, 'esrtoggle', true, 'tan_d', 0.001, 'debyetoggle', false, ... % Dielectric properties
    'skintoggle', true, ... % Skin effect
    'Lprofile', struct('shape', 'disabled'), ...
    'Rprofile', struct('shape', 'disabled'), ...
    'Cprofile', struct('shape', 'disabled'), ...
    'Gprofile', struct('shape', 'disabled') ...
);

connectors = {sma_coax};

[L0_line , R0_line, C0_line, G0_line, l] = TLprimaryParameters(connectors);

% Approximate propagation velocity
v_line = max(sqrt(1 ./ (L0_line .* C0_line))); % Propagation velocity [m/s]

%% Simulation parameters
% Also check TLfrequencySpan documentation)
sim_params = struct( ...
    'freqmode', 'signal',...
    'stimulusfile', stimulus_file,...
    'v_line', v_line, ...
    'l', sum(l) ...
);

%% Simulation parameters
freq_params = TLfrequencySpan(sim_params);

% Array length check
if freq_params.Nx >= 20e3
    array_check = questdlg(sprintf('LineLab >> Current settings require the simulation of %d cells. Continue anyway?', freq_params.Nx), 'LineLab long array');
    if ~strcmp(array_check, 'Yes')
        disp('LineLab >> Simulation aborted');
        return
    end
end

%% Profiling parameters

disp('LineLab >> Computing custom profiles for primary parameters')

% Extracting relevant quantities from freqParams struct
Nh = freq_params.Nh;
Nx = freq_params.Nx;
dx = freq_params.dx;
omega = freq_params.omega;

% Computing parameter profiles
Nxi = round(Nx*l/sum(l));
% FIXME find a cleaner solution to ensure sum(Nxi) = Nx
if sum(Nxi) - Nx ~= 0
    Nxi(1) = Nxi(1) - (sum(Nxi) - Nx);
end

L_line = zeros(1,Nx);
C_line = zeros(1,Nx);
R_line = zeros(1,Nx);
G_line = zeros(1,Nx);
for i = 1:length(Nxi)
    if i == 1
        istart = 1;
    else
        istart = Nxi(i-1)+1;
    end

    L_line(istart:istart+Nxi(i)-1) = L0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Lprofile);
    R_line(istart:istart+Nxi(i)-1) = R0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Rprofile);
    C_line(istart:istart+Nxi(i)-1) = C0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Cprofile);
    G_line(istart:istart+Nxi(i)-1) = G0_line(i) + TLcomposeProfile(Nxi(i), connectors{i}.Gprofile);
end

%% Solution (numeric)
% Performing frequency domain simulations
stopwatch = toc;

disp('LineLab >> Performing frequency domain simulations')

z11 = zeros(Nh+1, 1); z12 = zeros(Nh+1, 1);
z21 = zeros(Nh+1, 1); z22 = zeros(Nh+1, 1);
parfor kw = 1:Nh
    % Series dispersion
    YL = TLskinEffect(Nxi, connectors, omega(kw), R_line, L_line) / dx;
    % Parallel dispersion
    YC = TLepsilonDispersion(Nxi, connectors, omega(kw), G_line, C_line) * dx;
    
    [z11(kw+1), z12(kw+1), z21(kw+1), z22(kw+1)] = TLimpedanceMatrix(YL, YC);
end

% DC response
if sum(R_line) ~= 0
    [z11(1), z12(1), z21(1), z22(1)] = TLimpedanceMatrix(1./R_line, G_line);
else
    % TODO not implemented
end

fprintf('LineLab >> Simulation completed in %f s\n', toc - stopwatch)

%% Save impedance matrix

stopwatch = toc;
fprintf('LineLab >> Simulation completed. Total time elapsed: %d min %d sec\n', ...
    floor(stopwatch/60), round(mod(stopwatch, 60)))

%% Stimulus calibration procedure
% This script estimates the true stimulus, calibrating out mismatches
% between generator, connector and probe end

stimulus = load(stimulus_file);

t = stimulus.t; % Timebase
v2 = stimulus.v; % Measured stimulus

%% Into the frequency domain

% Array of frequencies
dt = mean(diff(t));
df = 1/t(end);
f_max = 1/dt;
Nh = length(t)/2 - 1;
w = 2*pi*df*(0:Nh)';

% Source and load impedances
ZS_w = ZS(w);
ZL_w = ZL(w);

%% Calibration
V2 = fft(v2)/length(v2);
Hright = (ZS_w + z11) .* (ZL_w + z22) ./ (ZL_w .* z21) - z12 ./ ZL_w;
if isnan(Hright(1))
    Hright(1) = 1;
end
H = [Hright; 0; conj(Hright(end:-1:2))];
VS = H .* V2;
v = ifft(VS) * length(v2);

%% Plot calibrated stimulus
plot(t, v, t, v2)
delay = mean(diff(t)) * finddelay(v, v2);
grid on
xlim([-4,4]*1e-9)
xlabel('t [s]', 'fontsize', 12, 'interpreter', 'latex')
ylabel('Waveform [V]', 'fontsize', 12, 'interpreter', 'latex')
legend({'Calibrated', 'Measured'}, 'fontsize', 12, 'interpreter', 'latex', 'location', 'best')
title(['Estimated delay: ', num2str(delay, '%.3e'), ' s'], 'fontsize', 12, 'interpreter', 'latex')

%% Save calibrated stimulus
vars = {'t', 'v', 'connectors'};
save(calibrated_filename, vars{:})

rmpath('./lib')
