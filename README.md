# README #

LineLab is a MATLAB-based computationally efficient Transmission Line simulator.
The development of LineLab is oriented to the simulation of Time Domain Reflectometry (TDR) systems, with the final goal of using it for data assimilation, i.e. TL parameter reconstruction from TDR measurements.
LineLab is an open source project started by Giuseppe Maria D'Aucelli, working at the [Measurement Group](http://misure.poliba.it/) of the Electric and Information Engineering department ([DEI](http://dei.poliba.it)), [Politecnico di Bari](http://www.poliba.it).

## What LineLab is about ##

* Frequency-domain 1-D TEM Transmission Line simulation
* Time Domain Reflectometry ([TDR](https://en.wikipedia.org/wiki/Time-domain_reflectometer)) simulation

## How do I get set up? ##

You simply need to clone the repository:

`git clone https://bitbucket.org/misurepoliba/linelab-simulator my-linelab-dir`

Where `my-linelab-dir` is where LineLab will be installed. No additional configuration steps are needed. Two main scripts are currently available:

* `TLadmittance.m`
* `TLtdrsim.m`

### Datafile compatibility ###

TODO

### Frequency domain simulation: `TLadmittance.m`###

The first script, `TLadmittance.m`, is used to simulate the response of the Transmission Line at its first node, given some basic parameters and the profiles of the primary parameters.

More details about the required parameters are thereafter provided.

#### Basic line parameters ####

In the cell named `%% Basic line parameters` a struct is initialized containing the basic line parameters that can be categorized in:

* Line model (currently only `'coax'` model is available)
* Line geometry (related to the line model)
* Dielectric properties (e.g. the dielectric constant `epsilon_r`)
* Dispersion properties (e.g. skin effect or dielectric relaxation)

#### Load and source parameters ####

Afterwards, in the `%% Load and source parameters` cell, the load and source parameters are set. The source impedance is (realistically) assumed to be a simple resistance, meanwhile the load impedance can assume any complex value. The parameters are:

* `Zload`: complex load impedance in the form `R + jX`
* `RS`: source resistance

#### Simulation parameters ####

In the `%% Simulation parameters` cell, a simple struct is defined which contains four parameters needed by LineLab to self-configure the frequency domain simulation engine. Required fields in the `simParams` struct are:

* `freqmode`: if set to `'signal'`, uses the Fourier analysis of the provided signal (i.e. its timebase) to set the spectrum up. This setting is recommended for TDR simulation using custom signals, since no resampling will be needed in the time domain. Otherwise, one can set this parameter to `'custom'` and provide any desired spectral range
* `stimulusfile`: is the path, including filename, of the datafile containing the signal which will be used to determine the spectral range of the simulation. If none is provided, set this to `'no_signal'`
* `v_line`: is a reasonable estimate of the propagation velocity of EM signals in the line, used for consistency checks
* `l`: is the length of the line, also mainly used for consistency checks
* `fmax`: the maximum frequency to be simulated. Not used in `'signal'` frequency mode
* `Nh`: the number of harmonics to be simulated. Also not used in `'signal'` frequency mode

#### Definition of parameter profiles ####

Finally, in the `%% Parameter profile definitions` cell the shapes of the primary parameters along the line are defined. This cell allows users to define custom shapes to

Detailed instruction about available shapes are given in the documentation of the `TLcomposeProfile.m` function.

### Time Domain Reflectometry simulation: `TLtdrsim.m`###

TODO

## Contribution guidelines ##

Currently the frequency-domain module of LineLab works just smoothly with any profile of the primary parameters (RLCG).

TODO

## Who do I talk to? ##

* Repo admin: Giuseppe Maria D'Aucelli ([giuseppe.daucelli@gmail.com](mailto:giuseppe.daucelli@gmail.com))
* Team website: [Poliba Measurement Group](http://misure.poliba.it/)
