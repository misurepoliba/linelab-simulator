%% Init
clear, close all
clc

tic % Start execution timer

savefile = './lines/line.mat'; % File path and name for simulated line

disp('LineLab >> Frequency domain simulation module')
addpath('./lib')

% Figures
plot_profiles = false;
plot_s11 = true; Z0 = 50;
tdr_toggle = false;

% Stimulus file
% If calibration data are available, they must be provided here as:
% - Zsource, omega_s: generator impedance
% - Zmeas, omega_m: measurement port impedance
stimulus_file = './measurements/lecroy/1_i2mtc_s.mat';
%stimulus_file = 'no_signal';
%cal_data = load(stimulus_file);
cal_data = struct();

% Line model
line_model_file = './line_models/rg58cu_1m.mat';

frequency_mode = 'custom';

% Measurement node, intended as the interface between subsequent lines
% E.g.:
%   0 -> at the beginning of the first line
%   1 -> between line 0 and 1
%   2 -> between line 1 and 2
measurement_port = 0;

% Used in custom frequency mode
t_max = 20e-8;
Nh = 1999;
%t_max = 10*20*Nh/1000/200e6;

%% Source, measurement port and load parameters
% Source impedance
if isfield(cal_data, 'Zsource')
    Zsource = @(w) interp1(cal_data.omega_s, cal_data.Zsource, w);
else
    Zsource = @(w) 50 + 1i*w*0;
end
% Measurement port impedance
if isfield(cal_data, 'Zmeas')
    Zmeas = @(w) interp1(cal_data.omega_m, cal_data.Zmeas, w);
else
    Zmeas = @(w) 1e9 ./ (1 + 1i*w*0e-12*1e6);
end
% Load impedance
if isfield(cal_data, 'Zload')
    Zload = @(w) interp1(cal_data.omega_l, cal_data.Zload, w);
else
    Zload = @(w) 1e9 ./ (1 + 1i*w*0e-12*1e9);
end

%% Basic line parameters

load(line_model_file)

if isfield(cal_data, 'connectors')
    network_topology = [cal_data.connectors, line_model];
else
    network_topology = line_model;
end

% Primary parameters and maximum propagation velocity
[L0_line , R0_line, C0_line, G0_line, l] = TLprimaryParameters(network_topology);
v_line = max(sqrt(1 ./ (L0_line .* C0_line)));

%% Simulation parameters
% Also check TLfrequencySpan documentation)
sim_params = struct( ...
    'freqmode', frequency_mode,...
    'stimulusfile', stimulus_file,...
    'v_line', v_line, ...
    'l', sum(l), ...
    'tmax', t_max, ...
    'Nh', Nh ...
);

%% Simulation parameters
freq_params = TLfrequencySpan(sim_params);

% Array length check
if freq_params.Nx >= 30e3
    array_check = questdlg(sprintf('LineLab >> Current settings require the simulation of %d cells. Continue anyway?', freq_params.Nx), 'LineLab long array');
    if ~strcmp(array_check, 'Yes')
        disp('LineLab >> Simulation aborted');
        return
    end
end

%% Profiling parameters

disp('LineLab >> Computing custom profiles for primary parameters')

% Extracting relevant quantities from freqParams struct
Nh = freq_params.Nh;
Nx = freq_params.Nx;
dx = freq_params.dx;
omega = freq_params.omega;

% Computing parameter profiles
Nxi = round(Nx*l/sum(l));
% FIXME find a cleaner solution to ensure sum(Nxi) = Nx
if sum(Nxi) - Nx ~= 0
    Nxi(1) = Nxi(1) - (sum(Nxi) - Nx);
end

L_line = zeros(1,Nx);
C_line = zeros(1,Nx);
R_line = zeros(1,Nx);
G_line = zeros(1,Nx);
for i = 1:length(Nxi)
    if i == 1
        istart = 1;
    else
        istart = Nxi(i-1)+1;
    end

    L_line(istart:istart+Nxi(i)-1) = L0_line(i) + TLcomposeProfile(Nxi(i), network_topology{i}.Lprofile);
    R_line(istart:istart+Nxi(i)-1) = R0_line(i) + TLcomposeProfile(Nxi(i), network_topology{i}.Rprofile);
    C_line(istart:istart+Nxi(i)-1) = C0_line(i) + TLcomposeProfile(Nxi(i), network_topology{i}.Cprofile);
    G_line(istart:istart+Nxi(i)-1) = G0_line(i) + TLcomposeProfile(Nxi(i), network_topology{i}.Gprofile);
end

%% Solution (numeric)
% Performing frequency domain simulations
stopwatch = toc;

if measurement_port == 0
    measurement_node = 1;
else
    measurement_node = round(sum(l(1:measurement_port)) / sum(l) * Nx);
end

VrefH = zeros(Nh, 1);
if(isempty(gcp('nocreate')))
    disp('LineLab >> Performing frequency domain simulations (single thread)')
    for kw = 1:Nh
        % Source, measurement and load impedances
        YS_w = 1/Zsource(omega(kw)); Ymeas_w = 1/Zmeas(omega(kw)); Yload_w = 1/Zload(omega(kw));
        % Series dispersion
        YL = TLskinEffect(Nxi, network_topology, omega(kw), R_line, L_line) / dx;
        % Parallel dispersion
        YC = TLepsilonDispersion(Nxi, network_topology, omega(kw), G_line, C_line) * dx;
        VrefH(kw) = TLsolveCircuit(YS_w, Ymeas_w, YL, YC, Yload_w, measurement_node);
    end
else
    disp('LineLab >> Performing frequency domain simulations (multi thread)')
    parfor kw = 1:Nh
        % Source, measurement and load impedances
        YS_w = 1/Zsource(omega(kw)); Ymeas_w = 1/Zmeas(omega(kw)); Yload_w = 1/Zload(omega(kw));
        % Series dispersion
        YL = TLskinEffect(Nxi, network_topology, omega(kw), R_line, L_line) / dx;
        % Parallel dispersion
        YC = TLepsilonDispersion(Nxi, network_topology, omega(kw), G_line, C_line) * dx;
        VrefH(kw) = TLsolveCircuit(YS_w, Ymeas_w, YL, YC, Yload_w, measurement_node);
    end
end

% DC response
% FIXME causes a lot of trouble: deprecate?
if sum(R_line) ~= 0 || sum(G_line) ~= 0
    VrefH0 = TLsolveCircuit(1/Zsource(0), 1/Zmeas(0), 1./R_line, G_line, 1/Zload(0), measurement_node);
else
    if isinf(Zload(0))
        VrefH0 = 1;
    else
        VrefH0 = Zload(0) / (Zload(0) + Zsource(0));
    end
end

% Merging DC and AC responses
freq_params.omega = [0; freq_params.omega]; VrefH = [VrefH0; VrefH];

fprintf('LineLab >> Simulation completed in %f s\n', toc - stopwatch)

%% Save data
disp('LineLab >> Saving simulation data on disk')

% Variables to be saved
vars = {'sim_params', 'network_topology', 'freq_params', ...
    'Zload', 'Zsource', 'Zmeas', 'VrefH'};
save(savefile, vars{:})

stopwatch = toc;
fprintf('LineLab >> Simulation completed. Total time elapsed: %d min %d sec\n', ...
    floor(stopwatch/60), round(mod(stopwatch, 60)))

%% Performing additional operations

% TODO all these quantities basically depend on just VrefH and omega. They might be computed by a single function, e.g. TLpostProcess(omega, VrefH, quantity)

% S11
IrefH = (1 - VrefH) ./ Zsource([0; omega]);
a = 0.5*(VrefH + Z0*IrefH) / sqrt(real(Z0));
b = 0.5*(VrefH - Z0*IrefH) / sqrt(real(Z0));

S11 = b ./ a;

% Voltage Standing Wave Ratio
VSWR = (1+abs(S11)) ./ (1-abs(S11));

% Group delay
tg = - diff(unwrap(angle(S11))) ./ diff(freq_params.omega);

%% Plotting useful variables

% RLCG profiles
if plot_profiles
    hProfiles = TLplotProfiles(R_line, L_line, C_line, G_line, l);
end

% S11
if plot_s11
    hS11 = TLplotComplex(freq_params.omega, S11, 'plotmode', 'mag', 'fontsize', 12);
end

%% Close program
rmpath('./lib')

if tdr_toggle
    TLtdrsim
end