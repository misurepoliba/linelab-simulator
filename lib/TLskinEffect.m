function YL = TLskinEffect(Nxi, line_parameters, omega, R_line, L_line)
%TLSKINEFFECT Summary of this function goes here
%   Detailed explanation goes here
%disp('LineLab >> Assembling matrices (skin effect)')

sigma_copper = 5.96e7; % Copper conductance
mu0 = 4*pi*1e-7; % Magnetic permeability

R = zeros(size(R_line));
L = zeros(size(L_line));
for i = 1:length(Nxi)
    
    if i == 1
        istart = 1;
    else
        istart = Nxi(i-1)+1;
    end
    
    switch line_parameters{i}.linemodel
        % Coaxial line
        case 'coax'
            if line_parameters{i}.skintoggle
                ri = line_parameters{i}.ri; ro = line_parameters{i}.ro;
                Rsheet = sqrt(omega*mu0/(2*sigma_copper));
                R(istart:istart+Nxi(i)-1) = Rsheet*(1/ri + 1/ro)/(2*pi) + R_line(istart:istart+Nxi(i)-1);
                % TODO implement inductive faults
                L(istart:istart+Nxi(i)-1) = (mu0/2/pi*log(ro/ri) + Rsheet / (2*pi*omega)*(1/ri + 1/ro))*ones(size(L_line(istart:istart+Nxi(i)-1)));
            else
                R(istart:istart+Nxi(i)-1) = R_line(istart:istart+Nxi(i)-1);
                L(istart:istart+Nxi(i)-1) = L_line(istart:istart+Nxi(i)-1);
            end

        case 'biwire'
            if line_parameters{i}.skintoggle
                d = line_parameters{i}.d;
                Rsheet = sqrt(omega*mu0/(2*sigma_copper));
                R(istart:istart+Nxi(i)-1) = 2*Rsheet/(pi*d) + R_line(istart:istart+Nxi(i)-1);
                L(istart:istart+Nxi(i)-1) = L_line(istart:istart+Nxi(i)-1);
            else
                R(istart:istart+Nxi(i)-1) = R_line(istart:istart+Nxi(i)-1);
                L(istart:istart+Nxi(i)-1) = L_line(istart:istart+Nxi(i)-1);
            end
    end
end

YL = 1 ./ (R + 1i*omega*L);

end

