function xp = TLcomposeProfile(Nx, params)
%TLCOMPOSEPROFILE Generates a custom profile for any line parameter X
%   TLcomposeProfile mainly requires the number Nx of samples to be
%   computed and a struct with other parameters. Other required parameters
%   depend on the chosen shape and are provided along with it in the struct.
%
%   Please note that almost any shape can be cumulated, i.e. a profile may
%   be obtained that is sum of suitably scaled and shifted profiles having
%   the same shape (e.g. the interfaces between three different dielectrics
%   can be defined by summing up two step or gauss_step profiles).
%   This feature is accessed by defining arrays of parameters in the params
%   struct
%
%   - 'poly_real': X(i) = kf(n+1)*i^n + kf(n)*i^(n-1) + ... kf(2)*i +
%   kf(1), with 1 <= i <= Nx
%   - TODO implement 'poly_trig' for trigonometric polynomials

% Converting relative parameters to indices
xp = zeros(Nx,1);
switch params.shape
    case 'point'
        for the_position = params.position
            xp(max([round(the_position * Nx), 1])) = params.amplitude;
        end
        
    case 'step'
        for i = 1:length(params.position)
            kfi = max([round(params.position(i) * Nx), 1]);
            xp(kfi:end) = xp(kfi:end) + params.amplitude(i);
        end
        
    case 'rect'
        for i = 1:length(params.position)
            wfi = max([round(params.width(i) * Nx), 1]);
            kfi = max([round(params.position(i) * Nx), 1]);
            xp(kfi - round(wfi/2):kfi + round(wfi/2)) = ...
                xp(kfi - round(wfi/2):kfi + round(wfi/2))+ params.amplitude(i);
        end
        
    case 'gauss_pulse'
        for i = 1:length(params.position)
            wfi = max([round(params.width(i) * Nx), 1]);
            kfi = max([round(params.position(i) * Nx), 1]);
            xfi = params.amplitude(i);
            xp = xp + xfi * normpdf((1:Nx)', kfi, wfi) ./ normpdf(kfi, kfi, wfi);
        end
        
    case 'gauss_rect'
        for i = 1:length(params.position)
            wfi = max([round(params.width(i) * Nx), 1]);
            kfi = max([round(params.position(i) * Nx), 1]);
            rfi = max([round(params.rise(i) * Nx), 1]);
            xfi = params.amplitude(i);
            xp = xp + xfi * (normcdf((1:Nx)', kfi - round(wfi/2), rfi) - ...
                normcdf((1:Nx)', kfi + round(wfi/2), rfi));
        end
        
    case 'gauss_step'
        for i = 1:length(params.position)
            kfi = max([round(params.position(i) * Nx), 1]);
            rfi = max([round(params.rise(i) * Nx), 1]);
            xfi = params.amplitude(i);
            xp = xp + xfi * normcdf((1:Nx)', kfi, rfi);
        end
        
    case 'poly_real'
        % TODO not implemented
        xp = zeros(1,Nx);
        for i = length(kf):-1:1
            xp = xp + kf(i)*(1:Nx).^(i-1);
        end
        
    case 'poly_fourier'
        coeff = params.coefficients;
        xp = coeff(1)*ones(Nx,1);
        for i = 2:2:length(coeff)
            xp = xp + coeff(i)*cos(2*pi/Nx*(i/2)*(1:Nx)') + ...
                coeff(i+1)*sin(2*pi/Nx*(i/2)*(1:Nx)');
        end
        
    case 'disabled'
        
    otherwise
        warning('TLcomposeProfile >> Unsupported profile shape ''%s''', shape)
        
end

end
