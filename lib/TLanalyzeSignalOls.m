function [ c0, a, b ] = TLanalyzeSignalOls( x, Np, Nh )
% TLANALYZESIGNALOLS Estimation of spectral
% parameters of a periodic signal via linear OLS The
% function estimates the spectral parameters assuming a
% known arbitrary (noninteger) number of periods Np, and for
% a given harmonic order Nh
%
% [ c0, a, b ] = TLanalyzeSignalOls( x, Np, Nh )
%
% Input variables:
% x = [Nx1] input signal
% Np = [1x1] number of samples
% Nh = [1x] harmonic order
%
% Output variables:
% c0 = [1x1] dc component
% a = [Nhx1] cosine components
% b = [Nhx1] sine components

%% Definition of auxiliary variables
N = length(x); % number of samples
h = (1:Nh)'; % column vector of harmonics indexes
n = (0:N-1)'; % column vector of sample indexes
Wx = 2*pi*Np/N; % digital (normalized) angular frequency

%% Linear OLS estimation
Theta = Wx*n*h'; % [NxNpar] matrix, argument of trigonometric functions
A = [ones(size(n)), cos(Theta), -sin(Theta)]; % matrix of base functions
p = A\x; % linear OLS estimation

%% Spectral parameters extraction
c0 = p(1); % dc
a = p(2:Nh+1); % cosine
b = p(Nh+2:end); % -sine

end
