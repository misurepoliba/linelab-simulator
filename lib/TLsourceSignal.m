function [ Vs ] = TLsourceSignal( sourceSignal, t, td, tr, A, varargin )
%TLSOURCESIGNAL Generates stimulus signal for transmission line simulation
%   Currently supported stimuli are:
%
%   - Gaussian pulse with delay td, width tr and amplitude A ('gauss_pulse')
%
%   - Gaussian step with delay td, width tr and amplitude A ('gauss_step')
%   
%   - Chirped gaussian pulse with chirp factor C supplied as additional parameter ('gauss_pulse_chirp')
%   
%   - Simple step ('step')
%   
%   - Exponential step ('exp_step')
%
%   - Exponential pulse ('exp_pulse')
%
%   - Arbitrary signal from CSV file ('arbitrary') FIXME a smarter import routine is needed

% Calculate source voltage
switch sourceSignal
    case 'gauss_step'
        Vs = A * normcdf(t, td, tr);
    case 'gauss_pulse'
        Vs = A * normpdf(t, td, tr) ./ normpdf(td, td, tr);
    case 'gauss_pulse_chirp'
        C = varargin(1);
        C = C{1};
        Vs = A * normpdf(t, td, tr) ./ normpdf(td, td, tr) .* cos(C*t/tr);
    case 'step'
        Vs = zeros(size(t));
        Vs(t > td) = A;
    case 'exp_step'
        Vs = A*(1 - exp(-(t - td)/tr));
        Vs(t < td) = 0;
    case 'exp_pulse'
        Vs = A / tr .* exp(-(t - td)/tr);
        Vs(t < td) = 0;
    case 'arbitrary'
        [csvfile, csvpath] = ...
            uigetfile({'*.csv', 'Comma separated values'});
        csvdatafile = [csvpath, csvfile];
        t = varargin(2);
        t = t{1};
        Vs = TLloadSignal(csvdatafile, t);
        Vs = Vs';
end
    
end