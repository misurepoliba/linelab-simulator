function [z11, z12, z21, z22] = TLimpedanceMatrix(YL, YC)
%TLIMPEDANCEMATRIX Summary of this function goes here
%   Detailed explanation goes here
Nx = length(YC);

% Tridiagonal vectors
bY = [0, -YL];
cY = [-YL, 0];

% z11, z21 (i2 = 0)
aY = zeros(Nx+1, 1) + 1i*0;
aY(1) = YL(1);
aY(2:end-1) = YL(1:end-1) + YC(1:end-1) + YL(2:end);
aY(end) = YL(end) + YC(end);

n = Nx+1;
f = zeros([n,1]) +1i*0; f(1) = 1;
v = zeros([n,1]) + 1i*0;
y = v;
w = aY(1);
y(1) = f(1)/w;
for i = 2:n
    v(i-1) = cY(i-1)/w;
    w = aY(i) - bY(i)*v(i-1);
    y(i) = ( f(i) - bY(i)*y(i-1) )/w;
end
for i = n-1:-1:1
    y(i) = y(i) - v(i)*y(i+1);
end

z11 = y(1); z21 = y(end);

% z12, z22 (i1 = 0)
aY = zeros(Nx+1, 1) + 1i*0;
aY(1) = YL(1);
aY(2:end-1) = YL(1:end-1) + YC(1:end-1) + YL(2:end);
aY(end) = YL(end) + YC(end);

n = Nx+1;
f = zeros([n,1]) +1i*0; f(end) = 1;
v = zeros([n,1]) + 1i*0;
y = v;
w = aY(1);
y(1) = f(1)/w;
for i = 2:n
    v(i-1) = cY(i-1)/w;
    w = aY(i) - bY(i)*v(i-1);
    y(i) = ( f(i) - bY(i)*y(i-1) )/w;
end
for i = n-1:-1:1
    y(i) = y(i) - v(i)*y(i+1);
end

z12 = y(1); z22 = y(end);

end
