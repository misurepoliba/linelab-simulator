function hFig = TLplotTDR(t, x, varargin)
%TLPLOTTDR Plots the simulated Time Domain Reflectometry response of a
%Transmission Line synthesized with LineLab
%   TLPLOTTDR also allows additional parameters as name - value pairs:
%   - 'fontsize' (scalar, numeric) adjusts the font size for axis labels
%   - 'integral' (vector, numeric) provides integrated reflectogram to be
%   plotted along with the simulated reflectogram
%   - 'measurements' (vector, numeric) provides measured data to be plotted
%   along with the simulated reflectogram
%   - 'xlim', in the form [t0, t1], defines a custom timebase limitation
%   for the plots

% Parse input name-value pairs
p = inputParser;
p.CaseSensitive = false;
addParameter(p, 'fontsize', 12, @isnumeric)
addParameter(p, 'linewidth', 2, @isnumeric)
addParameter(p, 'integral', [], @isnumeric)
addParameter(p, 'measurements', [], @isnumeric)
addParameter(p, 'xlim', [], @isnumeric)
parse(p, varargin{:})
inParams = p.Results;

if ~isempty(inParams.integral) && ~isempty(inParams.measurements)
    error('Please, provide only integrated waveforms or measurements')
end

fontsize = inParams.fontsize;
linewidth = inParams.linewidth;
integral = inParams.integral;
measurements = inParams.measurements;

hFig = figure;
labelOptn = {'interpreter', 'latex', 'fontsize', fontsize};

if ~isempty(integral)
    hPltMain = plot(t, x, t, integral); set(hPltMain, 'linewidth', linewidth)
    grid on
    xlabel('t [s]', labelOptn{:})
    ylabel('Reflectogram [V]', labelOptn{:})
    legend('Simulation', 'Integrated simulation', 'location', 'best')
else
    if ~isempty(measurements)
        hPltMain = plot(t, x, t, measurements, '--'); set(hPltMain, 'linewidth', linewidth)
        grid on
        xlabel('t [s]', labelOptn{:})
        ylabel('Reflectogram [V]', labelOptn{:})
        legend({'Simulation', 'Measurements'}, labelOptn{:}, 'location', 'best')
    else
        hPltMain = plot(t, x); set(hPltMain, 'linewidth', linewidth)
        grid on
        xlabel('t [s]', labelOptn{:})
        ylabel('Reflectogram [V]', labelOptn{:})
    end
end

if ~isempty(inParams.xlim)
    xlim(inParams.xlim)
end

end

