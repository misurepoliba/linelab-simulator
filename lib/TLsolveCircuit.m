function vh = TLsolveCircuit(YS, Ymeas, YL, YC, Yload, measurement_node)
%TLSOLVECIRCUIT Summary of this function goes here
%   Detailed explanation goes here
Nx = length(YC);

% Adding measurement port (lumped) admittance
YC(measurement_node) = YC(measurement_node) + Ymeas;

% Tridiagonal vectors (flipped for fast solution)
bY = [0, -YL(end:-1:1)];
cY = [-YL(end:-1:1), 0];

aY = zeros(Nx+1, 1) + 1i*0;
aY(1) = YS + YL(1);
aY(2:end-1) = YL(1:end-1) + YC(1:end-1) + YL(2:end);
aY(end) = YL(end) + YC(end) + Yload;
aY = aY(end:-1:1);

n = Nx+1;
f = zeros([n,1]) +1i*0; f(end) = YS;
v = zeros([n,1]) + 1i*0;
y = v;
w = aY(1);
y(1) = f(1)/w;
for i = 2:n
    v(i-1) = cY(i-1)/w;
    w = aY(i) - bY(i)*v(i-1);
    y(i) = ( f(i) - bY(i)*y(i-1) )/w;
end
if measurement_node > 1
    for i = n-1:-1:n-measurement_node+1
        y(i) = y(i) - v(i)*y(i+1);
    end
    vh = y(i);
else
    vh = y(end);
end


end
