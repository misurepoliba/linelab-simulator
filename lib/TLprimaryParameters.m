function [L0_line , R0_line, C0_line, G0_line, l] = TLprimaryParameters(par)
%TLPRIMARYPARAMETERS Computes basic primary parameters for some simple line models
%   Each different line model requires a different set of parameters, which
%   must be provided in the PAR struct. In detail:
%
%   - 'coax' line requires
%       - 'l' total length of the line [m]
%       - 'ro' outer conductor radius [m]
%       - 'ri' inner conductor radius [m]
%       - 't' outer conductor thickness [m]
%       - 'epsilon_r' relative dielectric permittivity of the inner material
%
%   - 'biwire' line is WIP
%       - 'l' total length of the line [m]
%       - 'd' diameter of wires [m]
%       - 'D' distance between centers [m]
%       - 'epsilon_r' effective dielectric permittivity of the surrounding material

epsilon0 = 8.854187817e-12; % Vacuum dielectric permittivity
mu0 = 4*pi*1e-7; % Vacuum magnetic permeability

disp('LineLab >> Computing primary line parameters')

C0_line = zeros(size(par));
L0_line = zeros(size(par));
R0_line = zeros(size(par));
G0_line = zeros(size(par));
l = zeros(size(par));
for i = 1:length(par)
    the_line = par{i};
    switch the_line.linemodel
        case 'coax'
            % Extracting parameters
            sigma = the_line.sigma; % Copper conductance
            ro = the_line.ro;
            ri = the_line.ri;
            t = the_line.t;
            epsilon_r = the_line.epsilon_r;

            C0_line(i) = 2*pi*epsilon0*epsilon_r / log(ro/ri); % Distributed capacitance [F/m]
            L0_line(i) = mu0/(2*pi) * log(ro/ri); % Distributed inductance [H/m]
            R0_line(i) = 1/(pi*ri^2*sigma) + 1/(pi*sigma)/((ro + t)^2 - ro^2); % Distributed resistance [ohm/m]
            G0_line(i) = 0; % Distributed conductance [S/m]
            
        case 'biwire'
            % Extracting parameters
            d = the_line.d;
            D = the_line.D;
            epsilon_r = the_line.epsilon_r;
            sigma_d = the_line.sigma_d;

            C0_line(i) = pi*epsilon0*epsilon_r / acosh(D/d); % Distributed capacitance [F/m]
            G0_line(i) = pi*sigma_d / acosh(D/d); % Distributed conductance [S/m]
            R0_line(i) = 2 / (pi * (d/2)^2 * sigma); % Distributed resistance [ohm/m]
            L0_line(i) = mu0 / pi * (1/4 + acosh(D/d)); % Distributed inductance [H/m]
            
        otherwise
            error('Unknown line model ''%s''', linemodel)
    end
    l(i) = the_line.l;
end

end

