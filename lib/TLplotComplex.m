function hFig = TLplotComplex(omega, X, varargin)
%TLPLOTS11 Plots a complex quantity in a single plot window
%   Additional input parameters can be given which specify, e.g. mag/ang or
%   real/imag modes ('mag', 'comp' are the only allowed values).
%
%   Accepted name-value pairs are:
%   - 'fontsize' (scalar, numeric) adjusts the font size for axis labels
%   - 'plotmode' (string) toggles between mag/ang ('mag') and real/imag ('real') plot modes

% Parse input name-value pairs
p = inputParser;
p.CaseSensitive = false;
addParameter(p, 'fontsize', 12, @isnumeric)
addParameter(p, 'plotmode', 'mag', @ischar)
parse(p, varargin{:})
inParams = p.Results;

fontsize = inParams.fontsize;
plotmode = inParams.plotmode;

hFig = figure;

switch plotmode
    case 'mag'
        subplot(2, 1, 1)
        hPlt = plot(omega/(2*pi)/1e9, 20*log10(abs(X))); set(hPlt, 'linewidth', 2)
        xlabel('Frequency [GHz]', 'interpreter', 'latex', 'fontsize', fontsize)
        ylabel('$|S_{11}|$ [dB]', 'interpreter', 'latex', 'fontsize', fontsize)
        grid on

        subplot(2, 1, 2)
        hPlt = plot(omega/(2*pi)/1e9, 180/pi*unwrap(angle(X))); set(hPlt, 'linewidth', 2)
        xlabel('Frequency [GHz]', 'interpreter', 'latex', 'fontsize', fontsize)
        ylabel('arg($S_{11}$) [deg]', 'interpreter', 'latex', 'fontsize', fontsize)
        grid on
        
    case 'real'
        subplot(2, 1, 1)
        hPlt = plot(omega/(2*pi)/1e9, real(X)); set(hPlt, 'linewidth', 2)
        xlabel('Frequency [GHz]', 'interpreter', 'latex', 'fontsize', fontsize)
        ylabel('$\Re[S_{11}]$', 'interpreter', 'latex', 'fontsize', fontsize)
        grid on

        subplot(2, 1, 2)
        hPlt = plot(omega/(2*pi)/1e9, imag(X)); set(hPlt, 'linewidth', 2)
        xlabel('Frequency [GHz]', 'interpreter', 'latex', 'fontsize', fontsize)
        ylabel('$\Im[S_{11}]$', 'interpreter', 'latex', 'fontsize', fontsize)
        grid on
end
        
end

