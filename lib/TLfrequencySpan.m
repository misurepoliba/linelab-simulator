function freqparams = TLfrequencySpan(par)
%TLFREQUENCYSPAN Computes the frequency span for the line responsonse
%simulation on the basis of the stimulus signal or over a custom spectral
%range. TLFREQUENCYSPAN also gives some useful time domain outputs
%
%   Detailed explanation goes here

freqmode = par.freqmode;
stimulusfile = par.stimulusfile;
v_line = par.v_line;
l = par.l;

switch freqmode
    case 'signal'
        disp('LineLab >> Loading stimulus data file and setting up frequencies')
        
        wave = load(stimulusfile);
        if isempty(find(contains(fieldnames(wave), 't'), 1)) || isempty(find(contains(fieldnames(wave), 'v'), 1))
            error('Cannot parse stimulus file');
        end
        
        t = wave.t;

        % Forcing even number of time-domain samples
        if mod(length(t), 2)
            t = t(1:end-1);
        end
        
        % Computing fft parameters
        t_max = range(t);
        Nh = length(t)/2 - 1;
        df = 1/t_max;
        omega = 2*pi*df*(1:Nh)';
        
        % TODO spatial discretization might be adaptive
        f_max = omega(end)/2/pi;
        lambda_min = v_line / f_max;
        dx = lambda_min / 10;
        Nx = ceil(l/dx);
        dx = l/Nx;
        
    case 'custom'
        disp('LineLab >> Setting up custom spectral range')
        
        Nh = par.Nh; % Number of frequencies
        t_max = par.tmax; % Total simulation time
        Nt = 2*(Nh+1); % Number of time-domain samples
        dt = t_max / Nt; % Time resolution [s]
        t = (0:dt:(Nt-1)*dt);
        
        df = 1 / t_max; % Frequency resolution [Hz]
        f_max = Nh * df; % Maximum frequency [Hz]
        
        lambda_min = v_line / f_max; % Minimum wavelength [m]
        dx = lambda_min / 10; % TODO customizable lambda_factor
        Nx = ceil(l / dx); % Number of cells
        dx = l / Nx; % Spatial resolution [m]
        omega = 2*pi*df*(1:Nh)';
        
        omega_max = 2*pi*v_line/lambda_min; % FIXME for consistency check      
end

freqparams = struct( ...
    'fmax', f_max, ...
    'df', df, ...
    'dx', dx, ...
    'omega', omega, ...
    'Nh', Nh, ...
    'Nx', Nx, ...
    'tmax', t_max, ...
    't', t ...
);

end

