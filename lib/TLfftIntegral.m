function X = TLfftIntegral(t, x)
%TLFFTINTEGRAL Summary of this function goes here
%   Detailed explanation goes here
N = length(t);
dw = 2*pi/range(t);
w = (0:N-1)'*dw;
x_fft = fft(x)/N;
X_fft = x_fft(2:N/2) ./ (1i*w(2:N/2));
X = ifft([0; X_fft; 0; conj(X_fft(end:-1:1))])*N + ...
    t * x_fft(1);
end

