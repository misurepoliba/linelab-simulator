function hFig = TLplotProfiles(R, L, C, G, l)
%TLPLOTPROFILES Plots the primary parameter profiles in a single figure
%   Detailed explanation goes here
Nx = length(R);
x = linspace(0, sum(l), Nx);
% TODO draw a dashed line at the interconnection points between lines

hFig = figure;

subplot(2,2,1);
hPlt = plot(x, R); set(hPlt, 'linewidth', 2)
grid on
xlabel('x [m]', 'interpreter', 'latex', 'fontSize', 12)
ylabel('R [$\Omega /$m]', 'interpreter', 'latex', 'fontSize', 12)

subplot(2,2,2);
hPlt = plot(x, L); set(hPlt, 'linewidth', 2)
grid on
xlabel('x [m]', 'interpreter', 'latex', 'fontSize', 12)
ylabel('L [H/m]', 'interpreter', 'latex', 'fontSize', 12)

subplot(2,2,3);
hPlt = plot(x, C); set(hPlt, 'linewidth', 2)
grid on
xlabel('x [m]', 'interpreter', 'latex', 'fontSize', 12)
ylabel('C [F/m]', 'interpreter', 'latex', 'fontSize', 12)

subplot(2,2,4);
hPlt = plot(x, G); set(hPlt, 'linewidth', 2)
grid on
xlabel('x [m]', 'interpreter', 'latex', 'fontSize', 12)
ylabel('G [S/m]', 'interpreter', 'latex', 'fontSize', 12)


end

