function YC = TLepsilonDispersion(Nxi, line_parameters, omega, G_line, C_line)
%TLEPSILONDISPERSION Summary of this function goes here
%   TODO only ESR currently implemented

C = zeros(size(C_line));
G = zeros(size(G_line));
for i = 1:length(Nxi)
    
    if i == 1
        istart = 1;
    else
        istart = Nxi(i-1)+1;
    end
    
    % Debye model
    if line_parameters{i}.debyetoggle
        % TODO not yet implemented
    else
        % ESR (tan_delta)
        if line_parameters{i}.esrtoggle
            tan_d = line_parameters{i}.tan_d;
            G(istart:istart+Nxi(i)-1) = tan_d*omega*C_line(istart:istart+Nxi(i)-1) + G_line(istart:istart+Nxi(i)-1);
            C(istart:istart+Nxi(i)-1) = C_line(istart:istart+Nxi(i)-1);
        % No dispersion
        else
            C(istart:istart+Nxi(i)-1) = C_line(istart:istart+Nxi(i)-1);
            G(istart:istart+Nxi(i)-1) = G_line(istart:istart+Nxi(i)-1);
        end
    end
end

YC = 1i*omega*C + G;

end

